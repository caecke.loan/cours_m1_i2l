# Agilité - Méthodes agiles
### Lien vers le cours : [Génie et Architecture Logicielle](http://www-lisic.univ-littoral.fr/~ramat/downloads/gal.pdf)

# I. Introduction 

**Risques d'un projet sans agilité :** diapo 149

**Variables d'ajustement d'un projet :**

    - Coût (Enveloppe de départ)
 
    - Qualité (expertise des équipes)

    - Durée (éviter les dépassements de délais)

    - Périmètre fonctionnel (quelles features sont demandées )


**Client :** Choisi 3 variables ( couts, qualité, durée )

**Devs :** Périmètre fonctionnel selon les variables du client.

# II. Le manifeste *Agile* 

## 1. Définition :

**Délivrer** de la meilleure façon possible ce qui a de la **Valeur** pour le **Client**.

## 2. Les 4 valeurs du manifeste :
  - **Personnes et interactions** : Plus que les processus et les outils.
  - **Logiciels qui fonctionne** : Plus qu'une documentation exhaustive.
  - **Collaboration avec le client** : Plus que la négociation contractuelle.
  - **Adaptation au changement** : Plus que le suivi d'un plan.

## 3. Les principes
  - **Satisfaire le client** par des **livraisons rapides et continues**.
  - **Intégrer les changements aux processus de dev** dans le but de garder une compétitivité du client.
  - **Livrer fréquemment** du code fonctionnel.
  - Client & dev main dans la main.
  - Equipes motivées (Environnement de travail correct, confiance, support nécessaire).
  - Conversations face à face.
  - Logiciel opérationnel = principale mesure de progrès.
  - **Rythme constant** à une vitesse "normale" et ce indéfiniment.
  - Attention continue sur **l'excellence technique**
  - **Simplicité** (minimiser au maximum le travail à faire)
  - **L'équipe se gère elle même** (Permet l'arrivée de meilleures architectures, exigences et designs)
  - **Régulièrement**, **l'équipe réfléchit a comment devenir + efficace**, + productif et adapte sa façon de faire en conséquence 


# II. DSDM

## 1. Principes
 - Implication des utilisateurs
 - **Autonomie** de l'équipe (pas de chef abstrait)
 - **Visiblité du résultat** : Livraison fréquente de l'appli pour récupérer des feedbacks.
 - **Adéquation** avec les besoins
 - **Développement itératif et incrémental** : Amélioration constante suite aux feedbacks de la précédente boucle.
 - **Réversibilité** , possibilité de revenir en arrière.
 - **Synthèse** à l'aide d'un schéma de direction du projet.
 - **Tests**, automatisés, pour éviter les regressions.
 - Coopération

# III. XP ( Extreme Programming ) 

## 1. Dates clés : 
**1999** - création

**2004** - XP v2

## 2. Valeurs : 
 -  Communication
 -  Simplicité
 -  Feedback
 -  Courage - S'adapter à un changement de rôle, de conception, de techno...
   (être capable de tout reprendre de zéro pour éviter le code "caché sous le tapis" à la livraison)
 -  Respect (v2) - Respecter et être respecté en tant que personne

## 3. Cycle XP

diapo 161


## 4. Les pratiques 
### 4.1 Feedback
  - Pair programming (transmission de connaissances/compétences)
  - Planning Game (séance de planification)
  - TDD
  - On-site customer

### 4.2 Processus continu
  - CI/CD
  - Refactoring continu
  - Frequent Small Releases (FSR)

### 4.3 Connaissances partagées
  - convention de codage
  - propriété collective du code
  - conception simple
  - Méthaphores

### 4.4 Bien-être du développeur 
    /!\ Pas plus de 40h par semaine /!\

### 4.5 Codage 
  - Client toujours disponible
  - Test unitaire en 1er
  - Pas d'heures supp
  - Optimisations à la fin

## 5. Les rôles

### 5.1 Développeur
  - Binôme, communique.
  - Autonome.
  - Double compétence : Développeur / concepteur.

### 5.2 Client
  - Exprime ses besoins dans des user-stories.
  - Possède une vision plus élevée sur le problème et l'environnement du métier.
  - Écrit les cas de tests fonctionnels.

### 5.3 Testeur
  - Aide le client à choisir et écrire ses tests fonctionnels.

### 5.4 Tracker
  - Aide à l'estimation des user story.
  - Contrôle l'avancement du planning.

### 5.5 Coach 
  - Un peu chef de projet.

### 5.6 Consultant
  - Apporte des connaissances pour la résolution de problème.

### 5.7 Big boss
  - Encourage les équipes.

# IV. Sofware craftsmanship

## 1. Définition 
  - Approche du dev avec l'accent sur les "coding skills"
  - **"Crafs-man"** : Un homme qui pratique son métier avec uen grande habileté
  - Manifeste : [https://manifesto.softwarecraftsmanship.org](https://manifesto.softwarecraftsmanship.org) 
  
## 2. Manifeste 
  - Logiciel qui fonctionne mais dans **les règles de l'Art**
  - Souple au changement mais aussi **création permanente de valeur**
  - Individus et intéraction, mais aussi **une communauté de progessionnels**
  - Collaboration des clients, mais aussi **partenariats productifs**

## 3. Résumé 
  - Plus un **mouvement** qu'une méthode
  - Agile **avec** pratiques techniques
  - Respect du rôle de l'ingénieur
  - Apprentissage et *mentoring*
  
## 4. Pratique 
  - Agile: XP, Scrum, Kanban
  - *SOLID*
  - TDD
  - Entrainement continu (Coding dojo, Code retreat)
  - *YAGNI*
  - Déploiement continu (CI/CD)

### 4.1 Solid
  - **Objectif** : rendre plus robuste une app
  - 1 classe = 1 responsabilité
    - responsabilité encapsulée dans la classe
  - Tous les services/méthodes doivent être **strictement** alignés sur la responsabilité
  - **Principe 1**: *Open-closed*
    - Open : Peut être étendue, spécialisée
    - Closed : Oblige abstraction/polymorphisme
  - **Principe 2**: *Liskov substitution*
    - Si S est un sous-type de T, alors tout objet de type T peut être remplacé par un objet de type S sans altérer les propriétés désirables du programme concerné.
      - -> Conception par contrat
  - **Principe 3**: *Interface segregation*
    - Ne garder que l'essentiel dans les interfaces et séparer les comportements dans différentes d'entre elles
    - En pratique : héritage multiple / adapters
  - **Principe 4**: *Dépendency inversion*
    - Les modules de haut niveau ne doivent pas dépendre de module de bas niveau
      - Les deux doivent dépendre d'abstractions
        - Les abstractions ne doivent pas dépendre des détails
        - Les détails doivent dépendre des abstractions
    - Plugin / Adapter (design pattern)

### 4.2 s'entrainer
  - **Kata :** Une personne démontre au reste du groupe à partir de 0 comment résoudre un problème.
  
  - **Coding dojo :**  Une rencontre entre plusieurs personnes sur un défide programmation collectif
  
  - **Code retreat :** 
    - Problème simple, fonctionnalité simple
    - 1 session de 45 minutes 
    - Durée totale de 9h à 16h
    - **Programmation en binôme obligatoire** (transfert de savoir)
    - De préférence, TDD
    - Après chaque session : 
      - Changement de binôme
      - Le code doit être totalement détruit
      - Un point de conception ou théorique est à approfondir

# V. SCRUM

## 1. Résumé

1. Processus pour produire la plus grande **valeur métier** dans la durée la plus courte
2. **Sprint** de 2 à 4 semaines de production qui résulte d'un logiciel fonctionnel
3. Métier = décrit les priorité  // équipe = choisi comment réaliser
4. Fin de sprint, livraison, préparation au nouveau sprint

## 2. Caractéristiques

1. Equipe autonome
2. Avancement par **sprints**
3. Exigences (features) listées dans un **backlog de produit**
4. Utilisation de règles génériques pour agiliser le projet

## 3. Rôles

1. Product owner (diapo 183)
2. Scrum master  (diapo 184)
3. Developpeurs

## 4. Equipe

1. 5 à 10 personnes
2. Couvre tous les rôles
3. S'auto organise
4. Ne change pas pendant un sprint

## 5. Cérémonial

### 5.1 Planification

1. Analyse/Evaluation backlog
2. Définition objectif sprint
3. Décisition d'architecture, techno, ...
4. Listing les tâches à partir des éléments du backlog
5. Estimation des tâches en heures (``Poker planning``)

### 5.2 Scrum quotidien (Daily meeting)
1. 15 minutes, tous les jours
2. Savoir où sont les dev dans leurs tâches
3. Tout le monde est invité mais seuls les membres de l'équipe peuvent parler
 
### 5.3 Review / Retrospective 

diapo 192

### 5.4 Artefact
- Backlog de produit 
- "User story" ( Scénario d'interaction avec le vocabulaire du client )
- "User story mapping" ( technique de création du backlog de produit )

Vidéo sur le story mapping :
[Le story mapping - La minute agile scrum (7min12s)](https://www.youtube.com/watch?v=2yPgR6OEo1s)