# Cours n°1 : Introduction
### [Lien vers le cours](http://www-lisic.univ-littoral.fr/~ramat/downloads/cours-LL-1.pdf)

# **0. Bibliographie**

> "Ethique hacker" - Steven Levy (Hackers: Heroes of the Computer Revolution)

> « The Art of Computer Programming » - Donald Ervin Knuth

> [1970 - no password - Une contre-histoire des Internets](https://www.youtube.com/watch?v=-7ZX-EQ6-9Y)

> [Fin années 70 du bourrage papier au logiciel libre - Une contre-histoire des Internets](https://www.youtube.com/watch?v=9nk--k0yk_s)

> [Reportage ARTE "Linux ou la communauté enfin libre"](https://www.youtube.com/watch?v=0JEgCYBo1H0)

> [Richard Stallman : Logiciels libres et éducation (liberté égalité fraternité)](https://www.youtube.com/watch?v=4KDMxzGC2Ag)


> [Eric Raymond : La Cathédrale et le Bazar (1997)](https://fr.wikipedia.org/wiki/La_Cath%C3%A9drale_et_le_Bazar)

> [Eric Raymond : A la conquête de la noosphère (1998)](http://www.linux-france.org/article/these/noosphere/)

> [Eric Raymond : Le chemin vers Hackerdom (1998)](https://doc.lagout.org/Others/Le%20Chemin%20vers%20Hackerdom.pdf)

> [Fred Brooks : "The Mythical Man-Month"](https://en.wikipedia.org/wiki/The_Mythical_Man-Month)


# 1. Personnages importants

- Théorisateur du monde des hackers : **Eric Raymond**
- Créateurs du mot "hacker" et du 1er labo d'IA au MIT : **Tech Model Railroad Club (asso étudiante)**
- Créateur d'UNIX / langage C : **Thompson / Ritchie**
- Cofondateur de Joy : **Bill Joy** 
- Créateur de Tex : **Ervin Knuth**
- Créateur de Perl : **Larry Wall**
- Créateur de Python : **Guido van Rossum**
- Le dieu du logiciel libre : **Richard Stallman**
- Créateur de Linux : **Linus Torvalds**

# 2. Qu'est ce qu'un hacker ? 
 - Définition
   - Quelqu'un qui cherche à résoudre des problèmes.
   - Quelqu'un qui crée, qui construit des choses.
   - **Croit en la liberté et en l'entraide bénévole.**

 - Valeurs
   - Toute information est par nature libre 
   - Pas de suivi aveugle de l'autorité
   - Jugement par les prouesses et exploits et pas d'autres hiérarchies sociales
   - Cherche à avoir le code le plus beau possible. Artiste & artisan du code.
  
# 3. Richard Stallman
  - 1970 : Harvard
  - 1971 : IBM New York & AI Lab du MIT. Ecriture d'un simulateur PDP-11.
  - 1975 : Quitte Havard, devient hacker au MIT.
  - 1984 : Projet GNU - Volonté de créer un système d'exploitation entièrement libre.
  - 1985 : Création de la Free Software Foundation, organisation à but non lucratif
    - Pour diffuser et financer les projets logiciel libre.
    - Pour sécuriser le logiciel libre à tous niveaux.
  - Développeur de Teco (Emacs), GCC, GDB.

# 4. Correction d'abus de langage

- Libre != domaine public
- Libre != gratuit, freeware (graticiel)
- Libre != shareware (partagiciel)
- Propriétaire != commercial
- Libre != accès au code source
- Libre != unix

# 5. Les Quatre Libertés Fondamentales du Libre

- Liberté d'**utilisation** d'un programme

- Liberté d'**étudier** le fonctionnement d'un programme

- Liberté de **modifier** un programme

- Liberté de **distribuer** un programme

# 6. Linus Torvalds

- 1980 : Découverte de l'informatique (à 11 ans)
- 1989 : Entrée à l'université et service militaire
- 1990 : Travail sur VMS, l'OS des VAX et déteste ça.
- Janvier 1991 : Achat d'un PC à base Intel 386
- Juillet 1991 : Appel aux dons pour avoir la définition de la norme POSIX
- Août 1991 : Demande les demandes de features aux utilisateurs de Minix
- 17 septembre 1991 : Première version de Linux (v0.01) 

## 6bis. Noyau Linux

- La méthode de fonctionnement (comme quasi tous les projets libres) :
  - Partir d'un code existant
  - Publier son code 
  - Avoir plusieurs utilisateurs ( et co-développeurs )
  - Laisser les utilisateurs / co-développeurs signaler les bugs, - proposer des solutions **et les intégrer**.
  - Mettre en avance publiquement ceux qui contribuent
  - Proposer très vite le code et mettre à jour fréquemment

  
# 7. Eric Raymond
  - Hacker
  - 1998 : Fondateur de l'Open Source Initiative
  - Développeur du **protocole SMTP**
  - Leader et théoricien du Logiciel à "Source ouvert" (**Open Source**)