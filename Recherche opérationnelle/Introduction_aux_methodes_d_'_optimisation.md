# Cours n°1 : Introduction
### [Lien vers le cours (à ajouter)](http://)

# **0. Bibliographie**

> Algorithmes Evolutionnaires (EA) - J. Holland 1975

> Algorithmes d'essaims particulaires (PSO) - R. Ebenhart & J. Kennedy 1995

> Algorithmes de fourmis (ACO) - Bonabeau 1999

> Algorithmes de descente - Hill-climber

> ... 

# **1. Définition**

Mis en forme mathématique d'un problème d'optimisation et résolution de ce dernier par une méthode informatique.

> **Problème d'optimisation** : Trouver la meilleure solution au problème selon un critère de qualité en modélisant la réalité (suppression des données inutiles)
> Un couple (X,f) avec **Espace de recherche** (ensemble des solutions possibles) et **fonction objectif** (critère de qualité)

# **2. Principe**

**Problème réel** -> modélisation -> **Problème d'optimisation** -> résolution -> **Solution(s)**


# **3. Heuristiques**

    Algo de résolution dont la conception repose sur l'expérience du concepteur.


# **4. Métaheuristiques**

    Métaheuristiques = Ensemble d'heuristiques.

- **Principes :**
    - Optimisation par essais/erreurs
    - Itération du processus essais/erreurs

**Solution unique :**
> Ex : Je regarde la solution courante, je modifie une variable, je remplace la solution courante si la qualité est meilleure.


**Population de solution (modélisation de la théorie de l'évolution de Darwin) :** 

    - Initialisation
        - Selection de variables
          - Changement random
            - Remplacement

# **5. Algorithme de recherche locale (Local Search)**

X = set of solutions ( espace de recherche)

f: X -> R objective function

v(x) set of neighbor's solutions of x


# **6. Algorithme d'exploitation maximale (Hill-Climber)**

- Algorithme de comparaison
- Opérateur local de base de métheuristique

- Principe :
  - Choisir une solution aléatoire.
  - Evaluer les voisines.
  - Garder en mémoire les solutions voisines meilleures.
  - Choisir une solution voisine meilleure aléatoirement.
  

# **7. Idée derrière la stratégie locale**

**Avantage** : Réduire la complexité d'un grand problème en un ensemble de petits problèmes.

**Inconvenient** : Risque de ne pas obtenir la meilleure solution.

    optimum local : X* est un optimum local ssi pour tout X € V(X*) , f(X) <= f(X*)
>

    optimum local strict : X est un optimum local ssi pour tout X € V(X*) , f(X) < f(X*)

>
    bassin d'attraction : X € B(X*) ssi il existe une suite de solutions X0,X1,X2,...,Xl tel que X0 = X et Xl = X*

# 8. Algos de recherches locales

## __8.1 Recuit Simulé__

Utilisé depuis les années 80.

Inspiré de la physique bollsman.

- **Biblio** : 
    - Metropolis (1953)
    - Kickpatrick et al. (IBM)
    - "Simulated Annealing and Boltzmann machine" John XX
    - " La méthode du recuit simulé : théorie et application " - P.Siarry

> **But** : Echapper aux optima locaux
> 
> **Principe** : Probabilité non nulle de sélection d'une solution voisine dégradée.


### *Décroissance de "température"*

Elle suit une loi géométrique **Tk+1 = αTk**

où 0.8 < **α** < 1.0


> **Remarques :** à l'heure actuelle, il existe des outils pour régler les paramètres (température,etc.) automatiquement.


## __8.2 Recherche tabou (Tabu Search)__

Glover - 1986

- **Biblio** : 
    - "Future paths for integer programming and links to artificial intelligence" - Fred Glover
    - "Tabu Search" - Kluwer Academic Publishers


> **But** : Echapper aux optima locaux
> 
> **Principe** : Introduction d'une notion de mémoire dans la stratégie d'exploration. (Empêche de reprendre des solutions déjà - ou recemment - rencontrées.)


  - Choisir une solution initiale s.
  - Initialiser Tabou M ( tableau contenant le nombre d'itérations bloqués /variable )
  - Choisir s' voisine de s telle que : 
    - soit s' est meilleure solution && critère d'aspiration vérifié
    - soit s' est meilleure solution && s' non tabou*
  - Update tabou M

> **Tabou :** Mouvement tabou/interdit pendant une durée.

- Lors d'un mouvement :
  - interdiction pendant n itérations 
  - n trop faible => tabou peu/pas efficace
  - n trop grand => solutions " à flanc de coteau "
  - n très puissant du **robust tabu search** =  random(N, 3N)

## __8.3 Recherche locale iterée (Iterated Local Search)__
> 
> **Principe** : Une fois la solution courante dans un optimum local, on fait une perturbation (grand modification) de la solution courante pour initier une novuelle recherche locale à partir de celle-ci.


- Choisir une solution initiale s.
- s <- localSearch(s)
- **Tant que** 
  - s' <- perturbation(s)
  - s' <- localSearch(s')
  - **si** F(s') > F(s)
    - s <- s'
- **Fin tant que**