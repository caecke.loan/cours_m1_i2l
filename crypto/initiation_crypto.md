# Initiation à la crypto

Cryptographie + Cryptoanalyse = Cryptologie

# Bibliographie

[Painvin et le "Radiogramme de la victoire"](http://www.bibmath.net/crypto/index.php?action=affiche&quoi=debvingt/radiogramme)

# **Définitions**
## ***Cryptographie***
Confidentialité, authenticité, intégrité
<br><br>

## ***Cryptanalyse***
Attaque d'un message chiffré
<br><br>

## ***Monoalphabetique***
Une même lettre = 1 seule même autre
<br><br>

## ***Polyalphabetique*** 
Une même lettre peut être chiffrée différemment
<br><br>


# Cryptanalyse

## **Etude de fréquence**

Etudie le pourcentage d'utilisation des lettres d'un message.

Dans un message chiffré de manière monoalphabétique, il est possible de décrypter le message grâce à la fréquence d'apparition des lettres chiffrées.

## **Indice de coïncidence**

Plus l'indice est haut, plus la valeur d'aléatoire diminue et plus on est proche d'avoir un alphabet monoalphabétique.

> Note : Un indice de 0.03 signifie un langage aléatoire (polyalphabétique).


## **Information sur la taille de la clé**

### **Le test de Kasiski**

- Recherche les différentes occurences entre des bouts répétés du message chiffré.
- Permet de détecter la taille de clé grâce à l'écart entre les occurences.

> Note : Très difficile à implémenter dans un langage de programmation puisque l'on ne connait pas la taille des occurences répétées.


# La cryptographie symétrique

## Principe


## Clé privée
  - Doit résister à une attaque par force brute.
  - Algo DES devenu obsolète ( devenue AES, clé de taille 128/256 à la place de 58 )

## Chiffrement à bloc 
### Fonctionnement
- Séparation en **blocs de même taille**
- Chiffrement bloc par bloc

### Schéma de Feistel
- Cg0 = Bloc de gauche à l'étape 0

    exemple : 
    
        texte 00101101, avec la clé 1010, F est un ou exclusif.
    
        Ronde 0 : 
        Cg0 : 0010
        Cd0 : 1101

        Ronde 1 :
        Cg1 : 1101
        Cd1 : 0101

        Ronde 2 :
        Cg2 : 0101
        Cd2 : 0010    

        Ronde 3 : 
        Cg3 : 0010
        Cd3 : 1101


# La cryptographie asymétrique (années 70)

C'est pas la même clé pour chiffrer et pour déchiffrer

Répond au problème de la crypto symétrique ( partage de la clé )


**Problèmes :** 
  - Lent 
  - Difficulté de gérénrer le couple de clé
 
 **Solution :**
 - Clé chiffrée de manière asymétrique 
 - Message chiffré de manière symétrique 

## RSA - 1977
  **Principe :**
   - Deux nombres premier distincts P et Q 
   - N = PQ
   - Fi(N) = valeur de l'indicatrice d'Euler en N.
   - Un exposant de chiffrement E
   - Un exposant de déchiffrement D
