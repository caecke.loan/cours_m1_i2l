# Cours n°1 : Introduction
### [Lien vers le cours (à ajouter)]()

# **0. Bibliographie**

# 1. **Les 5 principes de la sécurité**

## **a. Disponibilité**

> La ressource reste aussi accessible qu'annoncé. (*ex: Protection aux attaques DDOS*)

**Moyens mis en place** : Serveurs redondants, tests de montée en charge,...

## **b. Intégrité**

> Aucune information n'est modifiée entre sa production et sa reception. (*ex: Protection aux attaques Man In The Middle*)

**Moyens mis en place** : Réalisation d'une empreinte de données, reproduction de l'empreinte à la reception, insertion de métadonnées personnellles,...

## **c. Confidentialité**

> Une information n'est accessible que pour les acteurs concernés dans des conditions prédéfinies.

**Moyens mis en place** : Contrôle d'accès (physique ou logique), chiffrer les données,...

## **d. Authentification**

> Le système est capable de vérifier l'identité d'une entité et assure que seules les entités autorisées aient acccès à une composante du système.

**Moyens mis en place** : Mise en place d'un contrôle d'accès unique (login/mot de passe, empreinte digitale,..) + assurer l'inviolabilité des références d'accès stockées.


## **e. Non-repudiation**

> L'impossibilité de dissimuler ou nier une action effectuée par un auteur dans le système et possibilité de prouver qu'elle lui incombe.

**Moyens mis en place** : Mise en place de fichiers de logs impossibles à modifier, recourir à une authentification forte.

# 2. **Rappel des algos cryptographiques**

## **Algorithme à clef symétrique** 

> Clef partagée entre les correspondants pour chiffrer / déchiffrer un message.

### **Les + :**
- Rapide, confidentialité avec clef partagée.

### **Les - :**
- Problème d'échange de la clef

## **Algorithme de Diffie-Hellman** 

> Algorithme de calcul de la clé symétrique à partir d'informations différentes. ( Echange synchrone pour convenir d'une clef partagée. )

### **Les + :**
- Très efficace, pas de problème d'échange de clef

### **Les - :**
- inviolabilité statistique
- necessite la présence simultanée des 2 personnes.

## **Algorithme à clef asymétrique** 

> Chiffrement à deux clés ( 1 publique, 1 privée). Un message chiffré avec l'une des clefs ne peut être déchiffrée qu'avec l'autre clef. (RSA,DSA)

### **Les + :**
- Permet de faire à la fois de la confidentialité et de la signature (authentification).
- Ne necessite pas la présence simultanée des 2 personnes.

### **Les - :**
- Très lent, surtout pour la signature.

## **Hachage** 

> Empreinte non-inversible de taille fixe. Permet de répondre à la contrainte d'intégrité d'un paquet. (MD5,SHA,...)

### **Les + :**
- Répond à tous les problèmes de clef symétrique ou d'aléa de diffie-hellman.
- Réputée inviolable grâce au problème impossible à résoudre en temps raisonnable de déduction de clé privée via la clé publique ou inversement ( cas de (P => Q)  ou (Q => P))
- Très rapide à calculer.

### **Les - :**
- Ne garanti pas l'identité de l'expéditeur.


# 3. **Architecture PKI, certificats**

> **Problématique :**  Où trouver les informations de clefs publiques des entreprises et particuliers avec lesquels on souhaite communiquer ?

Les certificats de signature utilisés par les sites web sont générés par des organismes de confiance( comme une carte d'identité est générée par la préfecture de police) appelés **autorité**.

> **Note :** Le but d'une architecture PKI est de répondre à 4 des 5 principes clés de la sécurité informatique (confidentialité, authentification, intégrité, non-répudiation). Le seul facteur de risque restant étant de facteur humain.

## PKI (Public Key Infrastructure) / IGC (Infrastructure de Gestion de Clef)

C'est une infrastructure divisée en 5 autorités :
- **L'entité finale** : L'utilisateur final
- **L'autorité d'enregistrement** : Vérifie l'identité de l'utilisateur, distribue le certificat signé.
- **L'autorité de certification (AC/CA)** : Signe les demandes de certificat et de révocation
- **L'autorité de dépôt (Repository)** : Stocke  les certificats et de révocation
- **L'autorité de séquestre** : Stocke les clés de chiffrement générées par l'IGC pour pouvoir les restaurer le cas échéant.

## Application des certificats electroniques

- **Protocole SSL/TLS**
  - **Web :** HTTPS
  - **Mail :** SMTPS
  - **Accès distant messagerie :** POPS, IMAPS
  - **VPN :** IPSec
  - **Niveau applicatif :** login/pass
- **Chiffrement et/ou signature du courrier électronique** : S/MIME